import argparse
import ast
import re

# REG_EXP is a regexp for parsing input file contents
REG_EXP = re.compile(r'^.*\.py .*\.py$')


class Helper:
    @staticmethod
    def get_args() -> (str, str):
        argparser = argparse.ArgumentParser(
            prog='PyPlagiarism',
            description='Python CLI program for comparing .py scripts for equality using Levenshtein distance',
            epilog='(c)2023 Ivan Smaliakou')
        argparser.add_argument('in_file', type=str, help='input text file, where each line is expected to have 2 '
                                                         'filenames  of the .py script in the following format: '
                                                         'file1.py file2.py')
        argparser.add_argument('out_file', type=str, help='output text file, where each line contains the result of the'
                                                          'comparison of 2 scripts. It must be floating point number '
                                                          'between 0(not similar at all) and 1(identical)')
        args = argparser.parse_args()
        return args.in_file, args.out_file

    @staticmethod
    def parse_input(input_filename: str) -> dict:
        """
        :param input_filename: input file
        :return: dict(k: str, v: str) - key is original file name; copy is copy file name
        raises  ValueError if wrong input provided
        """
        matches = []
        with open(input_filename, "r") as input_file:
            for line in input_file:
                line = line.strip(' ')
                line = line.strip('\n')
                match = REG_EXP.match(line)
                matches.append(match.string)
        orig_to_copy = {}
        for match in matches:
            pair = match.split(' ')
            if len(pair) != 2:
                raise ValueError('wrong input: there should be only 2 space-separated items in each line')
            orig_to_copy[pair[0]] = pair[1]
        return orig_to_copy


class Comparator:
    def compare(self, orig: object, copy: object) -> float:
        """
        :param orig: original object
        :param copy: copy object
        :return: similarity ratio of copy(2nd param) relatively
        to orig(1st param) from 0(not similar at all) to 1(identical)
        """
        raise ValueError('not implemented')


class WordComparator(Comparator):
    def compare(self, obj1: object, obj2: object) -> float:
        raise ValueError('not implemented')


class LevenshteinComparator(WordComparator):
    # compare uses small optimization in favour of reducing memory usage, not reducing CPU usage.
    def compare(self, orig_word: str, copy_word: str) -> float:
        """
        :param orig_word: original root node
        :param copy_word: copy root node
        """
        # word1 is x; word2 is y
        try:
            orig_word = str(orig_word)
            copy_word = str(copy_word)
        except ValueError:
            return 0
        if orig_word == copy_word:
            return 1
        smaller_word, bigger_word = orig_word, copy_word
        if len(orig_word) > len(copy_word):
            smaller_word = copy_word
            bigger_word = orig_word
        current_line = [i for i in range(len(smaller_word) + 1)]
        for i in range(1, len(bigger_word) + 1):
            prev_line = current_line
            current_line = [i]
            for j in range(1, len(smaller_word) + 1):
                prev_min = min(current_line[j - 1], prev_line[j], prev_line[j - 1])
                if bigger_word[i - 1] == smaller_word[j - 1]:
                    current_line.append(prev_min)
                else:
                    current_line.append(prev_min + 1)
        return 1 - (current_line[len(smaller_word)] / len(copy_word))


class FileComparator(Comparator):
    def __init__(self, word_comparator: WordComparator):
        self.__word_comparator = word_comparator

    def compare(self, origfile: str, copyfile: str) -> float:
        with open(origfile, "r") as orig:
            orig_tree = ast.parse(orig.read())
        with open(copyfile, "r") as copy:
            copy_tree = ast.parse(copy.read())
        total_similarity_score, copy_tree_leaves_count = self.compare_nodes(orig_tree, copy_tree, 0)
        return total_similarity_score / copy_tree_leaves_count

    def compare_nodes(self, orig_root, copy_root, current_copy_leaves_count: int) -> (float, int):
        """
        compare_nodes parses abstract syntax tree and applies comparator.compare to each final node;
        Important: it's comparator task to check the validity of passed args
        :param orig_root: original root node
        :param copy_root: copy root node
        :param current_copy_leaves_count: currently computed count of the final leaves of the copy subtree.
        :return: tuple(x,y), where x-is the total score of similarity for the longest node, y is the count of the
        copy_root final leaves.
        """
        if orig_root is None and copy_root is None:
            return 1, current_copy_leaves_count + 1
        if orig_root is None or copy_root is None or type(orig_root) is not type(copy_root):
            return 0, current_copy_leaves_count + 1

        if isinstance(orig_root, ast.AST):  # handle basic case
            total_res = 0
            total_leaves_count = 0
            for k, v in vars(orig_root).items():
                if k in {"lineno", "end_lineno", "col_offset", "end_col_offset", "ctx"}:
                    continue
                current_res, current_leaves_count = self.compare_nodes(v, getattr(copy_root, k), 0)
                total_res += current_res
                total_leaves_count += current_leaves_count
            return total_res, total_leaves_count + current_copy_leaves_count

        elif isinstance(orig_root, list) and isinstance(copy_root, list):  # handle list case
            total_res = 0
            total_len = 0
            smallest_list_length = len(orig_root) if len(orig_root) < len(copy_root) else len(copy_root)
            length_diff = len(orig_root) + len(copy_root) - 2 * smallest_list_length
            for i in range(smallest_list_length):
                current_res, current_len = self.compare_nodes(orig_root[i], copy_root[i], 0)
                total_res += current_res
                total_len += current_len
            return total_res, total_len + length_diff + current_copy_leaves_count
        else:
            return self.__word_comparator.compare(orig_root, copy_root), current_copy_leaves_count + 1


def main():
    in_filename, out_filename = Helper.get_args()
    orig_to_copy = Helper.parse_input(in_filename)

    file_comparator = FileComparator(LevenshteinComparator())
    res = []
    for orig_filename, copy_filename in orig_to_copy.items():
        iter_res = file_comparator.compare(orig_filename, copy_filename)
        res.append(str(iter_res))

    with open(out_filename, 'w') as out_file:
        out_file.write('\n'.join(res))


if __name__ == '__main__':
    main()
