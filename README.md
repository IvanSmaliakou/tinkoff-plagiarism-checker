# Tinkoff plagiarism checker
_Now uses **Levenshtein distance-based algorithm** for checking 2 words for equality_<br>

## Dependencies
- #### python v3.8 or higher
## Usage:
    git clone git@gitlab.com:IvanSmaliakou/tinkoff-plagiarism-checker.git
    
    cd tinkoff-plagiarism-checker

    python3 compare.py input.txt output.txt

###### For more info you can call help:
    python3 compare.py --help
    